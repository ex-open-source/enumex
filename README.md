# Enumex

Advanced enum library.

## Installation

The package can be installed by adding `enumex` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:enumex, "~> 1.0"}
  ]
end
```

## Migrations

Migrations are one of most important features of `Enumex` library. Unlike others `enumex` not only provides helpful migration files, but all of them all fully reversible! Depends on use case you may need to add an extra values in order to make specific migration reversible. For more information please see: `Enumex.Migration`.

## Modules

Generated functions are split into few modules:

1. `MyEnum` - In main module you can find a most helpful functions like guards or those for fetching debug information.
2. `MyEnum.Absinthe` - This module defines enum type which could be used in projects with `absinthe` dependency.
3. `MyEnum.Constants` - Provides constant-like macros which is hepful for example in enabling hints in code editors.
4. `MyEnum.Migration` - Here you can find a shorter version functions of `Enumex.Ecto.Migration` prepared for your enum.
5. `MyEnum.Repo` - This module would need to be passed to some custom migrations. It allows to fetch information about enum from database.
6. `MyEnum.Type` - An up-to-date `Ecto.Type` implementation.

## Tasks

There are 2 mix tasks:

1. `enumex.gen.enum` which allows to quickly generate enum

```bash
$ mix enumex.gen.enum -m MyApp.MyEnum first second third
$ mix enumex.gen.enum -m MyApp.MyEnum first:5 second:10 third:15
```

2. `enumex.gen.migration` which automatically compares database and enum definition and generates migrations for:
a) Creating enum (if it does not exists)
b) Rename value (if identifiers are different, but indexes are same)
c) Add value (if it exists only in enum definition)
d) Drop value (if it exists only in database)

```bash
$ mix enumex.gen.migration -m MyApp.MyEnum my_enum_diff
```

## Usage

Simply `import` or `require` an `Enumex` module and use `enum` macros:

```elixir
defmodule MyEnum do
  import Enumex
  values = [:my, :enum]
  enum(values: values)
end
```

For more informations please take a look at `Enumex` module.

After declaring your enum alias it's `Type` module in your schema:

```elixir
defmodule MySchema do
  use Ecto.Schema

  alias Ecto.Changeset
  alias MyApp.MyEnum.Type # add this

  schema "my_table" do
    field :my_field, Type # add this
  end

  def changeset(my_element, my_params) do
    Changeset.cast(my_element, my_params, [:my_field]) # cast as any other field
  end
end
```

and migration:
```elixir
defmodule Example.Repo.Migrations.MyEnum do
  use Ecto.Migration

  alias MyApp.MyEnum.{Migration, Type} # add this

  def up do
    Migration.create_enum() # add this

    create table(:my_table) do
      add :my_field, Type.type() # add this
    end
  end

  def down do
    drop table(:my_table) # add this
    Migration.drop_enum() # add this
  end
end
```