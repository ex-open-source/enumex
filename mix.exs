defmodule Enumex.Mixfile do
  use Mix.Project

  @version "1.0.0"

  def application, do: [extra_applications: [:logger]]

  def project do
    [
      app: :enumex,
      deps: deps(),
      description: "Advanced enum library.",
      dialyzer: [
        ignore_warnings: "config/dialyzer_ignore.exs",
        plt_add_apps: [:absinthe, :ecto, :ecto_sql, :eex, :ex_unit, :mix, :optimus, :postgrex]
      ],
      docs: [
        extras: ["README.md": [filename: "enumex"]],
        main: "enumex",
        source_ref: "v#{@version}",
        source_url: "https://gitlab.com/ex-open-source/enumex"
      ],
      elixir: "~> 1.9",
      elixirc_paths: ["lib" | elixirc_paths(Mix.env())],
      name: "EctoPostgresEnum",
      package: [
        files: ~w(README.md lib mix.exs),
        licenses: ["MIT"],
        links: %{gitlab: "https://gitlab.com/ex-open-source/enumex"},
        maintainers: ["Tomasz Sulima"]
      ],
      preferred_cli_env: [
        coveralls: :test,
        "coveralls.html": :test,
        credo: :test,
        dialyzer: :test,
        docs: :docs
      ],
      test_coverage: [tool: ExCoveralls],
      version: @version
    ]
  end

  defp deps do
    [
      # code checks
      {:credo, "~> 1.1.0", only: :test, optional: true, runtime: false},
      {:dialyxir, "~> 1.0.0-rc.4", only: :test, optional: true, runtime: false},
      {:ex_doc, "~> 0.21", only: :docs, optional: true, runtime: false},
      {:excoveralls, "~> 0.10", only: :test, optional: true, runtime: false},
      # optional support
      {:absinthe, "~> 1.4.0", optional: true},
      {:ecto_sql, "~> 3.0", github: "elixir-ecto/ecto_sql", optional: true},
      {:postgrex, ">= 0.0.0", optional: true},
      # for tasks:
      {:optimus, "~> 0.1.8", optional: true}
    ]
  end

  defp elixirc_paths(:test), do: ["test/support"]
  defp elixirc_paths(_env), do: []
end
