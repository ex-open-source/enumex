defmodule Mix.Tasks.Enumex.Gen.Migration do
  use Mix.Task

  alias Enumex.Task
  alias Mix.Generator
  alias Mix.Tasks.Ecto.Gen.Migration

  require Generator

  @subcommand "enumex.gen.migration"
  @info Task.info(@subcommand)
  optimus = Optimus.new!(@info)
  @help Task.help(optimus)
  @version Optimus.Title.title(optimus)
  @moduledoc Enum.join(@help, "\n\n")
  @shortdoc Task.shortdoc(@subcommand)

  @aliases [h: :help, m: :module, r: :repo, v: :version]
  @ecto_switches [no_compile: :boolean, no_deps_check: :boolean, repo: [:string, :keep]]
  @switches [help: :boolean, module: :string, version: :boolean]

  def run(args) do
    parser_opts = [aliases: @aliases, strict: @ecto_switches ++ @switches]
    {opts, argv, rest} = OptionParser.parse(args, parser_opts)
    required = [:module, :repo]
    check = &Keyword.has_key?(opts, &1)

    cond do
      opts[:help] == true -> Enum.each(@help, &IO.puts/1)
      opts[:version] == true -> Enum.each(@version, &IO.puts/1)
      Enum.all?(required, &check.(&1)) and length(argv) == 1 and rest == [] -> success(opts, argv)
      true -> error(args)
    end
  end

  defp error(args) do
    {:ok, optimus} = Optimus.new(@info)
    {:error, subcommand, errors} = Optimus.parse(optimus, [@subcommand | args])
    Task.format_errors(optimus, subcommand, errors)
  end

  defp success(opts, [name]) do
    enum = Module.concat([opts[:module]])
    repo = Module.concat([enum, "Repo"])

    unless Code.ensure_loaded?(repo) do
      raise ArgumentError, "#{inspect(repo)} is invalid or was compiled without database support."
    end

    type = enum.prefixless_type()
    base_args = [adapter: enum.adapter(), name: type, prefix: enum.prefix(), repo: repo]
    opts |> get_repo() |> repo.get_values_with_index() |> do_success(base_args, enum, name, opts)
  end

  defp do_success({:error, %{postgres: %{pg_code: "42704"}}}, base_args, enum_module, name, opts),
    do: enum_module |> gen_create_enum(base_args) |> do_success(name, opts)

  defp do_success(database_values, base_args, enum_module, name, opts)
       when is_list(database_values) do
    enum_values = enum_module.values
    default = List.first(enum_values)
    %{indexes: indexes, sqls: rename} = gen_rename(enum_values, database_values, base_args)
    rest_enum_values = Enum.reject(enum_values, &(&1.index in indexes))
    rest_database_values = Enum.reject(database_values, &(&1.index in indexes))
    add = gen_add(rest_enum_values, rest_database_values, default, base_args)
    drop = gen_drop(rest_enum_values, rest_database_values, default, base_args)
    changes = add ++ drop ++ rename
    do_success(changes, name, opts)
  end

  defp do_success(changes, name, opts) do
    all_changes = ["alias Enumex.Ecto.Migration\n" | changes]
    change = all_changes |> Enum.map(&("    " <> &1)) |> Enum.join() |> String.trim_trailing()
    keys = Keyword.keys(@ecto_switches)
    ecto_opts = opts |> Keyword.take(keys) |> Enum.flat_map(&prepare_ecto_option/1)
    Migration.run(ecto_opts ++ ["--change", change, name])
  end

  defp get_repo(opts) do
    Application.ensure_all_started(:postgrex)
    Application.ensure_all_started(:ecto_sql)
    module_repo = opts |> Keyword.get(:repo) |> List.wrap() |> Module.concat()
    module_repo.start_link()
    module_repo
  end

  defp gen_add(enum_values, database_values, default, base_args) do
    enum_values
    |> Enum.reject(&(&1 in database_values))
    |> Enum.map(&do_gen_add(&1, default, base_args))
  end

  defp do_gen_add(value, default, base_args),
    do: add_value_template(base_args ++ [default: default, value: value])

  defp gen_create_enum(enum_module, base_args) do
    values = enum_module.values()
    [create_enum_template(base_args ++ [values: values]), gen_index_function(values, base_args)]
  end

  defp gen_drop(enum_values, database_values, default, base_args) do
    database_values
    |> Enum.reject(&(&1 in enum_values))
    |> Enum.map(&do_gen_drop(&1, default, base_args))
  end

  defp do_gen_drop(value, default, base_args),
    do: drop_value_template(base_args ++ [default: default, value: value])

  defp gen_rename(enum_values, database_values, base_args) do
    grouped_values = Enum.group_by(database_values, & &1.index)

    Enum.reduce(
      enum_values,
      %{indexes: [], sqls: []},
      &do_gen_rename(&1, &2, grouped_values, base_args)
    )
  end

  defp do_gen_rename(enum_value, %{indexes: indexes, sqls: sqls}, grouped_values, base_args) do
    value = List.first(grouped_values[enum_value.index] || [])

    if is_nil(value) or enum_value.atom == value.atom do
      %{indexes: indexes, sqls: sqls}
    else
      sql = rename_value_template(base_args ++ [new: enum_value, old: value])
      %{indexes: [value.index | indexes], sqls: [sql | sqls]}
    end
  end

  defp gen_index_function(values, base_args),
    do: create_index_function_template(base_args ++ [values: values])

  defp prepare_ecto_option({name, value}) do
    option = name |> Atom.to_string() |> String.replace("_", "-")
    ["--" <> option, value]
  end

  Generator.embed_template(:add_value, """
  Migration.add_value(<%= inspect(@adapter) %>, "<%= @prefix %>", "<%= @name %>", <%= inspect(@repo) %>, <%= inspect(@value, custom_options: [raw: true]) %>, <%= inspect(@default, custom_options: [raw: true]) %>)
  """)

  Generator.embed_template(:create_enum, """
  Migration.create_enum(<%= inspect(@adapter) %>, "<%= @prefix %>", "<%= @name %>", <%= inspect(@values, custom_options: [raw: true]) %>)
  """)

  Generator.embed_template(:create_index_function, """
  Migration.create_index_function(<%= inspect(@adapter) %>, "<%= @prefix %>", "<%= @name %>", <%= inspect(@values, custom_options: [raw: true]) %>)
  """)

  Generator.embed_template(:drop_value, """
  Migration.drop_value(<%= inspect(@adapter) %>, "<%= @prefix %>", "<%= @name %>", <%= inspect(@repo) %>, <%= inspect(@value, custom_options: [raw: true]) %>, <%= inspect(@default, custom_options: [raw: true]) %>)
  """)

  Generator.embed_template(:rename_value, """
  Migration.rename_value(<%= inspect(@adapter) %>, "<%= @prefix %>", "<%= @name %>", <%= inspect(@old, custom_options: [raw: true]) %>, <%= inspect(@new, custom_options: [raw: true]) %>)
  """)
end
