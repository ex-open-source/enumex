defmodule Mix.Tasks.Enumex.Gen.Enum do
  use Mix.Task

  alias Enumex.Task
  alias Mix.Generator

  require Generator

  @subcommand "enumex.gen.enum"
  @info Task.info(@subcommand)
  optimus = Optimus.new!(@info)
  @help Task.help(optimus)
  @version Optimus.Title.title(optimus)
  @moduledoc Enum.join(@help, "\n\n")
  @shortdoc Task.shortdoc(@subcommand)

  booleans = Enum.map([:help, :version], &{&1, :boolean})
  strings = Enum.map([:adapter, :file, :module, :prefix, :type], &{&1, :string})
  @strict booleans ++ strings
  first = fn {atom, _type} -> atom |> Atom.to_string() |> String.at(0) |> String.to_atom() end
  @aliases Enum.map(@strict, &{first.(&1), elem(&1, 0)})

  def run(args) do
    {opts, values, rest} = OptionParser.parse(args, aliases: @aliases, strict: @strict)

    cond do
      opts[:help] == true ->
        Enum.each(@help, &IO.puts/1)

      opts[:version] == true ->
        Enum.each(@version, &IO.puts/1)

      Keyword.has_key?(opts, :module) and values != [] and rest == [] ->
        do_run(opts, values)

      true ->
        {_value, skip_values} = List.pop_at(values, 0)
        all_args = if Keyword.has_key?(opts, :module), do: args, else: args -- skip_values
        optimus = Optimus.new!(@info)
        optimus |> Optimus.parse([@subcommand | all_args]) |> do_run(optimus)
    end
  end

  defp do_run({:error, subcommand, errors}, optimus),
    do: Task.format_errors(optimus, subcommand, errors)

  defp do_run(opts, values) do
    module = opts[:module]
    file = opts[:file] || file_name(module)
    keys = [:adapter, :prefix, :type]
    enum_opts = opts |> Keyword.take(keys) |> Enum.map(&prepare_enum_opt/1) |> Enum.join(", ")
    values_string = values |> Enum.map(&prepare_value/1) |> Enum.join("\n")
    template_opts = [module: module, opts: enum_opts, values: values_string]

    content =
      if enum_opts == "",
        do: enum_template(template_opts),
        else: enum_with_opts_template(template_opts)

    file |> Path.dirname() |> Generator.create_directory()
    Generator.create_file(file, content)
  end

  defp prepare_enum_opt({key, value}), do: "#{key}: " <> do_prepare_enum_opt(key, value)

  defp do_prepare_enum_opt(:adapter, adapter), do: adapter
  defp do_prepare_enum_opt(_key, value), do: value |> String.to_atom() |> inspect()

  defp file_name(module) do
    underscored = Macro.underscore(module)
    Path.join("lib", underscored) <> ".ex"
  end

  defp prepare_value(value), do: value |> String.split(":") |> do_prepare_value()

  defp do_prepare_value([string]), do: value_template(string: string)

  defp do_prepare_value([string, index]) do
    case Integer.parse(index) do
      {integer, _rest} -> value_with_index_template(index: integer, string: string)
      :error -> raise ArgumentError, "Cannot parse integer: #{index}"
    end
  end

  Generator.embed_template(:enum, """
  defmodule <%= @module %> do
    @moduledoc "An enum generated using enumex.gen.enum mix task."

    import Enumex

    enum do
  <%= @values %>
    end
  end
  """)

  Generator.embed_template(:enum_with_opts, """
  defmodule <%= @module %> do
    @moduledoc "An enum generated using enumex.gen.enum mix task."

    import Enumex

    enum(<%= @opts %>) do
  <%= @values %>
    end
  end
  """)

  Generator.embed_template(:value, ~s[    value(:<%= @string %>)])

  Generator.embed_template(
    :value_with_index,
    ~s[    value(:<%= @string %>, index: <%= @index %>)]
  )
end
