defmodule Enumex.Debug do
  @moduledoc false

  def block, do: [guards(), rest()]

  defp guards do
    quote do
      module = inspect(__MODULE__)
      first_atom = @__atom_values__ |> List.first() |> inspect()
      first_string = @__string_values__ |> List.first() |> inspect()
      first_value = @__values__ |> List.first() |> inspect(custom_options: [raw: true])

      @doc """
      Checks if given `atom` is allowed.

      ## Examples

          iex> #{module}.is_valid_atom(#{first_atom})
          true

          iex> #{module}.is_valid_atom(#{first_string})
          false

          iex> #{module}.is_valid_atom(#{first_value})
          false

          iex> #{module}.is_valid_atom(0)
          false
      """
      defguard is_valid_atom(value) when value in @__atom_values__

      @doc """
      Checks if given `string` is allowed.

      ## Examples

          iex> #{module}.is_valid_string(#{first_atom})
          false

          iex> #{module}.is_valid_string(#{first_string})
          true

          iex> #{module}.is_valid_string(#{first_value})
          false

          iex> #{module}.is_valid_string(0)
          false
      """
      defguard is_valid_string(value) when value in @__string_values__

      @doc """
      Checks if given `value` is allowed.

      ## Examples

          iex> #{module}.is_valid_value(#{first_atom})
          false

          iex> #{module}.is_valid_value(#{first_string})
          false

          iex> #{module}.is_valid_value(#{first_value})
          true

          iex> #{module}.is_valid_value(0)
          false
      """
      defguard is_valid_value(value) when value in @__values__

      @doc """
      Checks if given atom or string is allowed.

      ## Examples

          iex> #{module}.is_valid(#{first_atom})
          true

          iex> #{module}.is_valid(#{first_string})
          true

          iex> #{module}.is_valid(#{first_value})
          true

          iex> #{module}.is_valid(0)
          false
      """
      defguard is_valid(value)
               when is_valid_atom(value) or is_valid_string(value) or is_valid_value(value)
    end
  end

  defp rest do
    quote do
      module = inspect(__MODULE__)

      @doc """
      Returns adapter.

          iex> #{module}.adapter()
          #{inspect(@__adapter__)}
      """
      @spec adapter :: module | nil
      def adapter, do: @__adapter__

      @doc """
      Returns list of allowed atom values.

          iex> #{module}.atom_values()
          #{inspect(@__atom_values__)}
      """
      @spec atom_values :: [t_atom]
      def atom_values, do: @__atom_values__

      @doc """
      Returns prefix.

          iex> #{module}.prefix()
          #{inspect(@__prefix__)}
      """
      @spec prefix :: atom | nil
      def prefix, do: @__prefix__

      @doc """
      Same as `type/0`, but without prefix.

          iex> #{module}.prefixless_type()
          #{inspect(@__type__)}
      """
      @spec prefixless_type :: atom
      def prefixless_type, do: @__type__

      @doc """
      Returns list of allowed string values.

          iex> #{module}.string_values()
          #{inspect(@__string_values__)}
      """
      @spec string_values :: [t_string]
      def string_values, do: @__string_values__

      @doc """
      Returns zipped list of atom values with list of string values.

          iex> #{module}.values()
          #{inspect(@__values__, custom_options: [raw: true])}
      """
      @spec values :: [Enumex.Value.t()]
      def values, do: @__values__
    end
  end
end
