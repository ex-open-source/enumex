defmodule Enumex.Index do
  @moduledoc false

  def block, do: [at(), atom_at(), get_index(), string_at(), value_at()]

  defp at do
    quote unquote: false do
      module = inspect(__MODULE__)

      %{atom: first_atom, index: first_index, string: first_string} =
        first_value = List.first(@__values__)

      @doc """
      Finds the value at the given `index`.

      Returns `default` if `index` is out of bounds.

      ## Examples

          iex> #{module}.at(#{first_index}, :atom)
          #{inspect(first_atom)}

          iex> #{module}.at(#{first_index}, :string)
          #{inspect(first_string)}

          iex> #{module}.at(#{first_index})
          #{inspect(first_value, custom_options: [raw: true])}

          iex> #{module}.at(-1, :value)
          nil

          iex> #{module}.at(-1, :value, :not_found)
          :not_found
      """
      @spec at(-1 | non_neg_integer, :atom | :string | :value, Enum.default()) ::
              atom | String.t() | Enumex.Value.t() | Enum.default()
      def at(index, type \\ :value, default \\ nil)

      def at(-1, _type, default), do: default

      for %{atom: atom, index: index, string: string} = value <- @__values__ do
        value_macro = Macro.escape(value)
        def at(unquote(index), :atom, _default), do: unquote(atom)
        def at(unquote(index), :string, _default), do: unquote(string)
        def at(unquote(index), :value, _default), do: unquote(value_macro)
      end

      def at(index, _type, default) when index > 0, do: default
    end
  end

  defp atom_at do
    quote unquote: false do
      module = inspect(__MODULE__)

      %{atom: first_atom, index: first_index, string: first_string} =
        first_value = List.first(@__values__)

      @doc """
      Same as `at/3`, but for atom only.

          iex> #{module}.atom_at(#{first_index})
          #{inspect(first_atom)}

          iex> #{module}.atom_at(-1)
          nil

          iex> #{module}.atom_at(-1, :not_found)
          :not_found
      """
      @spec atom_at(-1 | non_neg_integer) :: t_atom | Enum.default()
      def atom_at(index, default \\ nil)
      def atom_at(-1, default), do: default
      def atom_at(index, default) when index >= 0, do: at(index, :atom, default)
    end
  end

  defp get_index do
    quote unquote: false do
      module = inspect(__MODULE__)

      %{atom: first_atom, index: first_index, string: first_string} =
        first_value = List.first(@__values__)

      @doc """
      Returns the `index` of the `atom`, `string` or `value`.

      Returns `-1` if `term` is not a part of enum.

      ## Examples

          iex> #{module}.get_index(#{inspect(first_atom)})
          #{first_index}

          iex> #{module}.get_index(#{inspect(first_string)})
          #{first_index}

          iex> #{module}.get_index(#{inspect(first_value, custom_options: [raw: true])})
          #{first_index}

          iex> #{module}.get_index(nil)
          -1
      """
      @spec get_index(Enumex.Value.t()) :: -1 | non_neg_integer
      @spec get_index(t_string) :: -1 | non_neg_integer

      for %{atom: atom, index: index, string: string} = value <- @__values__ do
        value_macro = Macro.escape(value)
        @spec get_index(unquote(atom)) :: unquote(index)
        def get_index(unquote(atom)), do: unquote(index)
        def get_index(unquote(string)), do: unquote(index)
        def get_index(unquote(value_macro)), do: unquote(index)
      end

      @spec get_index(term) :: -1
      def get_index(_term), do: -1
    end
  end

  defp string_at do
    quote unquote: false do
      module = inspect(__MODULE__)

      %{atom: first_atom, index: first_index, string: first_string} =
        first_value = List.first(@__values__)

      @doc """
      Same as `at/3`, but for string only.

          iex> #{module}.string_at(#{first_index})
          #{inspect(first_string)}

          iex> #{module}.string_at(-1)
          nil

          iex> #{module}.string_at(-1, :not_found)
          :not_found
      """
      @spec string_at(-1 | non_neg_integer) :: t_string | Enum.default()
      def string_at(index, default \\ nil)
      def string_at(-1, default), do: default
      def string_at(index, default) when index >= 0, do: at(index, :string, default)
    end
  end

  defp value_at do
    quote unquote: false do
      module = inspect(__MODULE__)

      %{atom: first_atom, index: first_index, string: first_string} =
        first_value = List.first(@__values__)

      @doc """
      Same as `at/3`, but for value only.

          iex> #{module}.value_at(#{first_index})
          #{inspect(first_value, custom_options: [raw: true])}

          iex> #{module}.value_at(-1)
          nil

          iex> #{module}.value_at(-1, :not_found)
          :not_found
      """
      @spec value_at(-1 | non_neg_integer) :: Enumex.Value.t() | Enum.default()
      def value_at(index, default \\ nil)
      def value_at(-1, default), do: default
      def value_at(index, default) when index >= 0, do: at(index, :value, default)
    end
  end
end
