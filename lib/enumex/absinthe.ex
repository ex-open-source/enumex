defmodule Enumex.Absinthe do
  @moduledoc false

  def block do
    if Code.ensure_loaded?(Absinthe) do
      quote unquote: false do
        defmodule Absinthe do
          import Enumex, only: []
          import Enumex.Value, only: []
          use Elixir.Absinthe.Schema.Notation

          parent = __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
          absinthe = Module.get_attribute(parent, :__absinthe__)
          name = Module.get_attribute(parent, :__type__)
          values = Module.get_attribute(parent, :__values__)

          default_moduledoc = """
          Helper module for working with `absinthe` enum.

          Usage:

              defmodule MyAppWeb.Schema
                use Absinthe.Schema

                import_types(#{inspect(__MODULE__)})

                # use #{inspect(name)} inside fields, mutations and queries
              end
          """

          @moduledoc absinthe[:moduledoc] || default_moduledoc

          @desc absinthe[:desc]
          values_ast = Enum.map(values, &{:value, [], [&1.atom, &1.absinthe]})
          enum_ast = {:enum, [], [name, [do: {:__block__, [], values_ast}]]}
          Code.eval_quoted(enum_ast, [], __ENV__)
        end
      end
    end
  end
end
