defmodule Enumex.Ecto.Type do
  @moduledoc false

  def block do
    quote do
      defmodule Type do
        @moduledoc "An Ecto.Type behaviour implementation."

        use unquote(__MODULE__)
      end
    end
  end

  defmacro __using__(_opts \\ []) do
    [base(), cast(), dump(), embed_as(), equal?(), load(), type()]
  end

  defp base do
    quote unquote: false do
      use Ecto.Type

      @parent __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
      @values Module.get_attribute(@parent, :__values__)
      @full_type Module.get_attribute(@parent, :__full_type__)

      first = List.first(@values)
      last = List.last(@values)
      value = &inspect(&1, custom_options: [raw: true])

      @first_atom inspect(first.atom)
      @first_string inspect(first.string)
      @first_value value.(first)
      @last_atom inspect(last.atom)
      @last_string inspect(last.string)
      @last_value value.(last)
      @module inspect(__MODULE__)
    end
  end

  defp cast do
    quote unquote: false do
      @doc """
      Casts the given input to the custom type.

      This is callback implementation for: `c:Ecto.Type.cast/1`

      ## Examples

          iex> #{@module}.cast(#{@first_atom})
          {:ok, #{@first_value}}

          iex> #{@module}.cast(#{@first_string})
          {:ok, #{@first_value}}

          iex> #{@module}.cast(5)
          :error
      """
      @impl Ecto.Type
      @spec cast(unquote(@parent).t) :: {:ok, Enumex.Value.t()}
      @spec cast(unquote(@parent).t_string) :: {:ok, Enumex.Value.t()} | :error
      for %{atom: atom, string: string} = value <- @values do
        escaped_value = Macro.escape(value)
        def cast(unquote(atom)), do: {:ok, unquote(escaped_value)}
        def cast(unquote(string)), do: {:ok, unquote(escaped_value)}
      end

      @spec cast(term) :: :error
      def cast(_term), do: :error
    end
  end

  defp dump do
    quote unquote: false do
      @doc """
      Dumps the given term into an Ecto native type.

      This is callback implementation for: `c:Ecto.Type.dump/1`

      ## Examples

          iex> #{@module}.dump(#{@first_atom})
          {:ok, #{@first_string}}

          iex> #{@module}.dump(#{@first_string})
          {:ok, #{@first_string}}

          iex> #{@module}.dump(#{@first_value})
          {:ok, #{@first_string}}

          iex> #{@module}.dump(5)
          :error
      """
      @impl Ecto.Type
      @spec dump(unquote(@parent).t) :: {:ok, unquote(@parent).t_string}
      @spec dump(unquote(@parent).t_string) :: {:ok, unquote(@parent).t_string} | :error
      @spec dump(Enumex.Value.t()) :: {:ok, unquote(@parent).t_string} | :error
      for %{atom: atom, string: string} = value <- @values do
        escaped_value = Macro.escape(value)
        def dump(unquote(atom)), do: {:ok, unquote(string)}
        def dump(unquote(string)), do: {:ok, unquote(string)}
        def dump(unquote(escaped_value)), do: {:ok, unquote(string)}
      end

      @spec dump(term) :: :error
      def dump(_term), do: :error
    end
  end

  defp embed_as do
    quote unquote: false do
      @doc """
      Dictates how the type should be treated inside embeds.

      This is callback implementation for: `c:Ecto.Type.embed_as/1`

      ## Examples

          iex> #{@module}.embed_as(:self)
          :self

          iex> #{@module}.embed_as("self")
          ** (FunctionClauseError) no function clause matching in #{@module}.embed_as/1
      """
      @impl Ecto.Type
      @spec embed_as(atom) :: :self
      def embed_as(format) when is_atom(format), do: :self
    end
  end

  defp equal? do
    quote unquote: false do
      @doc """
      Checks if two terms are semantically equal.

      This is callback implementation for: `c:Ecto.Type.equal?/2`

      ## Examples

          iex> #{@module}.equal?(#{@first_value}, #{@first_value})
          true

          iex> #{@module}.equal?(#{@first_value}, #{@last_value})
          false

          iex> #{@module}.equal?(#{@first_value}, #{@first_atom})
          false

          iex> #{@module}.equal?(#{@first_value}, #{@first_string})
          false

          iex> #{@module}.equal?(#{@first_value}, 1)
          false
      """
      @impl Ecto.Type
      @spec equal?(term, term) :: boolean

      for value <- @values do
        escaped_value = Macro.escape(value)

        def equal?(unquote(escaped_value), unquote(escaped_value)), do: true
      end

      def equal?(_term1, _term2), do: false
    end
  end

  defp load do
    quote unquote: false do
      @doc """
      Loads the given term into a custom type.

      This is callback implementation for: `c:Ecto.Type.load/1`

      ## Examples

          iex> #{@module}.load(#{@first_string})
          {:ok, #{@first_value}}

          iex> #{@module}.load(#{@first_atom})
          :error

          iex> #{@module}.load(5)
          :error
      """
      @impl Ecto.Type
      @spec load(unquote(@parent).t_string) :: {:ok, Enumex.Value.t()} | :error
      for %{string: string} = value <- @values do
        escaped_value = Macro.escape(value)
        def load(unquote(string)), do: {:ok, unquote(escaped_value)}
      end

      @spec load(term) :: :error
      def load(_value), do: :error
    end
  end

  defp type do
    quote unquote: false do
      @doc """
      Returns the underlying schema type for the custom type.

      This is callback implementation for: `c:Ecto.Type.type/0`

          iex> #{@module}.type()
          #{inspect(@full_type)}
      """
      @impl Ecto.Type
      @spec type :: atom
      def type, do: @full_type
    end
  end
end
