defmodule Enumex.Ecto.Query do
  @moduledoc false

  def block do
    quote do
      if Code.ensure_loaded?(Ecto.Query.API) and not is_nil(@__adapter__) do
        defmodule Query do
          @moduledoc "Helper functions for fetching data from database."

          parent = __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
          @name parent |> Module.get_attribute(:__type__) |> Atom.to_string()

          @doc "A helper function for calling SQL version of function which fetches index."
          defmacro get_index(prefix, string, func_name \\ "enumex_get_index_#{@name}") do
            quote do: fragment(unquote("#{prefix}.#{func_name}(?)"), unquote(string))
          end
        end
      end
    end
  end
end
