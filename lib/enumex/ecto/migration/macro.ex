defmodule Enumex.Ecto.Migration.Macro do
  @moduledoc false

  def block do
    quote do
      if Code.ensure_loaded?(Ecto.Migration) and not is_nil(@__adapter__) do
        defmodule Migration do
          @moduledoc "Helper functions for ecto migration files."

          use unquote(__MODULE__)
        end
      end
    end
  end

  defmacro __using__(_opts \\ []) do
    quote do
      alias Enumex.Ecto.Migration
      alias Enumex.Value

      parent = __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
      @adapter Module.get_attribute(parent, :__adapter__)
      @name parent |> Module.get_attribute(:__type__) |> Atom.to_string()
      @prefix parent |> Module.get_attribute(:__prefix__) |> Atom.to_string()
      @repo Module.concat(parent, Repo)
      @values Module.get_attribute(parent, :__values__)

      value = &inspect(&1, custom_options: [raw: true])

      base_doc = "Short version of #{inspect(unquote(__MODULE__))}"
      first_value = @values |> List.first() |> value.()
      last_value = @values |> List.last() |> value.()
      module = inspect(__MODULE__)
      new_name = inspect("new_name")
      new_value = Value.new(:new, 3, parent)
      old_value = Value.new(:old, 3, parent)
      second_value = @values |> Enum.at(1) |> value.()
      values_string = inspect(@values, custom_options: [raw: true])

      @doc """
      #{base_doc}.add_value/6

          #{module}.add_value(#{new_value})
          #{module}.add_value(#{new_value}, #{first_value})
      """
      @spec add_value(Value.t(), Value.t() | nil) :: :ok | no_return
      def add_value(value, default \\ nil),
        do: Migration.add_value(@adapter, @prefix, @name, @repo, value, default)

      @doc """
      #{base_doc}.create_enum/4

         #{module}.create_enum()
      """
      @spec create_enum() :: :ok | no_return
      def create_enum, do: Migration.create_enum(@adapter, @prefix, @name, @values)

      @doc """
      #{base_doc}.create_index_function/5

          #{module}.create_index_function()
      """
      @spec create_index_function() :: :ok | no_return
      def create_index_function,
        do: Migration.create_index_function(@adapter, @prefix, @name, @values)

      @doc """
      #{base_doc}.drop_enum/3

          #{module}.drop_enum()
      """
      @spec drop_enum() :: :ok | no_return
      def drop_enum, do: Migration.drop_enum(@adapter, @prefix, @name, @values)

      @doc """
      #{base_doc}.drop_index_function/4

          #{module}.drop_index_function()
      """
      @spec drop_index_function() :: :ok | no_return
      def drop_index_function,
        do: Migration.drop_index_function(@adapter, @prefix, @name, @values)

      @doc """
      #{base_doc}.drop_value/6

          #{module}.drop_value(#{last_value}, #{first_value})
      """
      @spec drop_value(Value.t(), Value.t()) :: :ok | no_return
      def drop_value(value, default),
        do: Migration.drop_value(@adapter, @prefix, @name, @repo, value, default)

      @doc """
      #{base_doc}.rename_enum/5

          #{module}.rename_enum(#{new_name})
      """
      @spec rename_enum(String.t()) :: :ok | no_return
      def rename_enum(new_name), do: Migration.rename_enum(@adapter, @prefix, @name, new_name)

      @doc """
      #{base_doc}.rename_value/5

          #{module}.rename_value(#{old_value}, #{new_value})
      """
      @spec rename_value(Value.t(), Value.t()) :: :ok | no_return
      def rename_value(old, new), do: Migration.rename_value(@adapter, @prefix, @name, old, new)
    end
  end
end
