defmodule Enumex.Ecto.Migration do
  @moduledoc "Generic functions for performing migrations for database enum."

  alias Ecto.Adapters.SQL, warn: false
  alias Ecto.Migration
  alias Enumex.Adapters.Postgres
  alias Enumex.Value

  require Migration

  @default_func_name "enumex_get_index"

  value = &Value.new(&1, &2, MyApp.ExampleEnum)

  adapter = inspect(Postgres)
  first_value = value.(:first, 1)
  last_value = value.(:last, 3)
  module = inspect(__MODULE__)
  name = inspect("example_enum")
  new_name = inspect("new_name")
  new_value = value.(:new, 3)
  old_name = inspect("old_name")
  old_value = value.(:old, 3)
  prefix = inspect("my_app_prefix")
  repo_string = ["MyApp.ExampleEnum.Repo"] |> Module.concat() |> inspect()
  second_value = value.(:second, 2)
  values_string = inspect([first_value, second_value])

  @doc """
  Adds a value to database enum.

      #{module}.add_value(#{adapter}, #{prefix}, #{name}, #{repo_string}, #{new_value})

  **Note**: `default` argument is required only if you want to have a reversible migration.
    
      #{module}.add_value(#{adapter}, #{prefix}, #{name}, #{repo_string}, #{new_value}, #{
    first_value
  })
  """
  @spec add_value(module, String.t(), String.t(), module, Value.t(), Value.t() | nil) ::
          :ok | no_return
  def add_value(adapter, prefix, name, repo, value, default \\ nil)

  def add_value(adapter, prefix, name, repo, value, nil) do
    up = fn -> do_add_value(adapter, prefix, name, repo, value) end
    Migration.execute(up, &not_reversible/0)
  end

  def add_value(adapter, prefix, name, repo, value, default) do
    up = fn -> do_add_value(adapter, prefix, name, repo, value) end
    down = fn -> do_drop_value(adapter, prefix, name, repo, value, default) end
    Migration.execute(up, down)
  end

  @doc false
  def do_add_value(adapter, prefix, name, repo, value) do
    db_values = repo.get_values_with_index(Migration.repo())
    nearest = Enum.find(db_values, &(&1.index > value.index))
    sql = sql_add_value(adapter, prefix, name, value, nearest)
    SQL.query!(Migration.repo(), sql, [], log: :info)
  end

  defp sql_add_value(adapter, prefix, name, value, nil),
    do: adapter.add_value(prefix, name, value.string)

  defp sql_add_value(adapter, prefix, name, value, nearest),
    do: adapter.add_value(prefix, name, value.string, "before", nearest.string)

  @doc """
  Creates a database enum with specific values.

      #{module}.create_enum(#{adapter}, #{prefix}, #{name}, #{values_string})
  """
  @spec create_enum(module, String.t(), String.t(), [Value.t()]) :: :ok | no_return
  def create_enum(adapter, prefix, name, values) do
    up = do_create_enum(adapter, prefix, name, values)
    down = do_drop_enum(adapter, prefix, name)
    Migration.execute(up, down)
  end

  defp do_create_enum(adapter, prefix, name, values) do
    string_values = Enum.map(values, & &1.string)
    adapter.create_enum(prefix, name, string_values)
  end

  @doc """
  Creates a SQL function for fetching enum value index.

      #{module}.create_index_function(#{adapter}, #{prefix}, #{name}, #{values_string})
  """
  @spec create_index_function(module, String.t(), String.t(), [Value.t()]) ::
          :ok | no_return
  def create_index_function(adapter, prefix, name, values) do
    up = do_create_index_function(adapter, prefix, name, values)
    down = do_drop_index_function(adapter, prefix, name)
    Migration.execute(up, down)
  end

  defp do_create_index_function(adapter, prefix, name, values) do
    values = Enum.map(values, &{&1.string, &1.index})
    adapter.create_index_function(prefix, @default_func_name <> "_" <> name, name, values)
  end

  @doc """
  Drops a database enum.

      #{module}.drop_enum(#{adapter}, #{prefix}, #{name})

  **Note**: `values` argument is required only if you want to have a reversible migration.

      #{module}.drop_enum(#{adapter}, #{prefix}, #{name}, #{values_string})
  """
  @spec drop_enum(module, String.t(), String.t(), [Value.t()] | nil) :: :ok | no_return
  def drop_enum(adapter, prefix, name, values \\ nil) do
    up = do_drop_enum(adapter, prefix, name)

    if is_nil(values) do
      Migration.execute(up)
    else
      down = do_create_enum(adapter, prefix, name, values)
      Migration.execute(up, down)
    end
  end

  defp do_drop_enum(adapter, prefix, name), do: adapter.drop_enum(prefix, name)

  @doc """
  Drops a SQL function for fetching enum value index.

      #{module}.drop_index_function(#{adapter}, #{prefix}, #{name})

  **Note**: `values` argument is required only if you want to have a reversible migration.

      #{module}.drop_index_function(#{adapter}, #{prefix}, #{name}, #{values_string})
  """
  @spec drop_index_function(module, String.t(), String.t(), [Value.t()] | nil) :: :ok | no_return
  def drop_index_function(adapter, prefix, name, values \\ nil) do
    up = do_drop_index_function(adapter, prefix, name)

    if is_nil(values) do
      Migration.execute(up)
    else
      down = do_create_index_function(adapter, prefix, name, values)
      Migration.execute(up, down)
    end
  end

  defp do_drop_index_function(adapter, prefix, name, func_name \\ nil) do
    func_name = func_name || @default_func_name <> "_" <> name
    adapter.drop_index_function(prefix, func_name, name)
  end

  @doc """
  This function safely drops an enum value.

      #{module}.drop_value(#{adapter}, #{prefix}, #{name}, #{repo_string}, #{last_value}, #{
    first_value
  })

  **Note**: In order to drop value this function need to drop index function too, so after
  dropping value please make sure to recreate index function.

      #{module}.drop_value(#{adapter}, #{prefix}, #{name}, #{repo_string}, #{last_value}, #{
    first_value
  })
      #{module}.create_index_function(#{adapter}, #{prefix}, #{name}, #{values_string})
  """
  @spec drop_value(module, String.t(), String.t(), module, Value.t(), Value.t()) ::
          :ok | no_return
  def drop_value(adapter, prefix, name, repo, value, default) do
    up = fn -> do_drop_value(adapter, prefix, name, repo, value, default) end
    down = fn -> do_add_value(adapter, prefix, name, repo, value) end
    Migration.execute(up, down)
  end

  @doc false
  def do_drop_value(adapter, prefix, name, repo, value, default) do
    db_values = repo.get_values(Migration.repo())
    values = for string <- db_values, string != value.string, do: %{string: string}
    tmp_name = "enumex_tmp_" <> name
    columns = repo.get_enum_columns(Migration.repo())

    [
      do_rename_enum(adapter, prefix, name, tmp_name),
      do_create_enum(adapter, prefix, name, values),
      Enum.map(columns, &do_convert(&1, adapter, name, value, default)),
      do_drop_index_function(adapter, prefix, tmp_name, "enumex_get_index_" <> name),
      do_drop_enum(adapter, prefix, tmp_name)
    ]
    |> List.flatten()
    |> Enum.each(&SQL.query!(Migration.repo(), &1, [], log: :info))
  end

  defp do_convert(map, adapter, name, value, default) do
    %{table_schema: schema, table_name: table, column_name: column} = map
    adapter.convert(schema, table, column, name, value.string, default.string)
  end

  @doc """
  Rename a database enum. This function also allows to change database prefix.

      #{module}.rename_enum(#{adapter}, #{prefix}, #{old_name}, #{new_name})
  """
  @spec rename_enum(module, String.t(), String.t(), String.t()) :: :ok | no_return
  def rename_enum(adapter, prefix, name, new_name) do
    up = do_rename_enum(adapter, prefix, name, new_name)
    down = do_rename_enum(adapter, prefix, new_name, name)
    Migration.execute(up, down)
  end

  defp do_rename_enum(adapter, prefix, name, new_name),
    do: adapter.rename_enum(prefix, name, new_name)

  @doc """
  Rename a database enum value.

      #{module}.rename_value(#{adapter}, #{prefix}, #{name}, #{old_value}, #{new_value})
  """
  @spec rename_value(module, String.t(), String.t(), Value.t(), Value.t()) ::
          :ok | no_return
  def rename_value(adapter, prefix, name, old, new) do
    up = do_rename_value(adapter, prefix, name, old, new)
    down = do_rename_value(adapter, prefix, name, new, old)
    Migration.execute(up, down)
  end

  defp do_rename_value(adapter, prefix, name, old, new),
    do: adapter.rename_value(prefix, name, old.string, new.string)

  @not_reversible_message "cannot reverse migration command: add_value. " <>
                            "You will need to explicitly define up/0 and down/0 in your migration " <>
                            "or make sure that you provide arguments which allows reversing this command."

  defp not_reversible, do: raise(Ecto.MigrationError, @not_reversible_message)
end
