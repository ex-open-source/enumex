defmodule Enumex.Ecto.Repo do
  @moduledoc false

  defmacro __using__(_opts \\ []), do: [get_enum_columns(), get_values(), get_values_with_index()]

  def block do
    quote do
      if Code.ensure_loaded?(Ecto.Repo) and not is_nil(@__adapter__) do
        defmodule Repo do
          @moduledoc "Helper functions for fetching data from database."

          @parent __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
          @adapter Module.get_attribute(@parent, :__adapter__)
          @name @parent |> Module.get_attribute(:__type__) |> Atom.to_string()
          @prefix @parent |> Module.get_attribute(:__prefix__) |> Atom.to_string()

          use Enumex.Ecto.Repo
        end
      end
    end
  end

  defp get_enum_columns do
    quote do
      @doc "Fetches prefix, table and column for specific enum usage in database."
      @spec get_enum_columns(module) :: list(map)
      def get_enum_columns(repo) do
        sql = @adapter.get_enum_columns(@prefix, @name)
        result = repo.query(sql)

        case result do
          {:ok, %Postgrex.Result{columns: columns, rows: rows}} ->
            list = Enum.map(columns, &String.to_atom/1)
            Enum.map(rows, &(list |> Enum.zip(&1) |> Map.new()))

          error ->
            error
        end
      end
    end
  end

  defp get_values do
    quote do
      @doc "Retunrs a list of database values. Useful if fetching index is not needed or impossible."
      @spec get_values(module) :: [String.t()]
      def get_values(repo) do
        sql = @adapter.get_values(@prefix, @name)
        result = repo.query(sql)

        case result do
          {:ok, %Postgrex.Result{rows: rows}} -> List.flatten(rows)
          error -> error
        end
      end
    end
  end

  defp get_values_with_index do
    quote do
      @doc "Fetches all enum values and their indexes from database."
      @spec get_values_with_index(module) :: Enumex.Value.t()
      def get_values_with_index(repo) do
        sql = @adapter.get_values_with_index(@prefix, "enumex_get_index" <> "_" <> @name, @name)
        result = repo.query(sql, [], log: :info)

        func = fn [string, index] ->
          %Enumex.Value{
            atom: String.to_atom(string),
            enum: @parent,
            index: index,
            string: string
          }
        end

        case result do
          {:ok, %Postgrex.Result{rows: rows}} -> Enum.map(rows, &func.(&1))
          error -> error
        end
      end
    end
  end
end
