defmodule Enumex.Base do
  @moduledoc false

  alias Enumex.Ecto.Migration.Macro, as: Migration
  alias Enumex.Ecto.{Query, Repo, Type}
  alias Enumex.{Absinthe, Constant, Debug, Index}

  defmacro __before_compile__(_env) do
    core = [block(), Constant.block(), Debug.block(), Index.block()]
    ecto = [Migration.block(), Query.block(), Repo.block(), Type.block()]
    [core, Absinthe.block(), ecto]
  end

  def block do
    quote unquote: false do
      @__absinthe__ @__opts__[:absinthe] || [desc: "", moduledoc: false]
      is_list(@__absinthe__) || raise "Absinthe options should be a list!"
      @__adapter__ @__opts__[:adapter]
      is_atom(@__adapter__) || raise "Adapter option must be an module!"
      Enumex.Base.valid_adapter?(@__adapter__) || raise("Adapter is not yet loaded!")
      @__prefix__ @__opts__[:prefix]
      is_atom(@__prefix__) || raise "Option prefix must be an atom!"
      @__type__ @__opts__[:type] || Enumex.Base.gen_default_type(__MODULE__)
      is_atom(@__type__) || raise "Type needs to be an atom!"
      @__full_type__ if is_nil(@__prefix__), do: @__type__, else: :"#{@__prefix__}.#{@__type__}"
      @__values__ Enum.sort_by(@__values__, & &1.index)
      @__atom_values__ Enum.map(@__values__, & &1.atom)
      @__string_values__ Enum.map(@__values__, & &1.string)
      @type t :: Enumex.Value.t() | t_atom | t_string
      @type t_atom :: unquote(Enumex.Base.gen_type_ast(@__atom_values__))
      @type t_string :: String.t()
    end
  end

  def gen_default_type(module),
    do: module |> Module.split() |> List.last() |> Macro.underscore() |> String.to_atom()

  def gen_type_ast(values), do: values |> Enum.reverse() |> do_gen_type_ast()

  defp do_gen_type_ast([head | tail]), do: Enum.reduce(tail, head, &{:|, [], [&1, &2]})

  def valid_adapter?(nil), do: true
  def valid_adapter?(adapter) when is_atom(adapter), do: Code.ensure_loaded?(adapter)
  def valid_adapter?(_), do: false
end
