defmodule Enumex.Adapters.Postgres do
  @moduledoc """
  A PostgreSQL adapter for `Enumex.Adapter` behaviour.

  ## Usage

  Simply set `adapter` option to `#{inspect(__MODULE__)}` like below:

      defmodule ExampleEnum do
        import Enumex

        enum [adapter: Enumex.Adapters.Postgres, …] do
          # …
        end
      end
  """

  @behaviour Enumex.Adapter

  @add_value_sql "alter type ${1}.${2} add value '${3}';"
  @add_value_with_type_sql "alter type ${1}.${2} add value '${3}' ${4} '${5}';"

  def add_value(prefix, name, new_value, type \\ nil, value \\ nil)

  def add_value(prefix, name, new_value, nil, nil),
    do: fragment(@add_value_sql, [prefix, name, new_value])

  def add_value(prefix, name, new_value, type, value),
    do: fragment(@add_value_with_type_sql, [prefix, name, new_value, type, value])

  @convert_enum_sql """
  alter table ${1}.${2} alter column ${3} set data type ${4} using (
    case ${3}::text
      when '${5}' then '${6}'
      else ${3}::text
    end
  )::${4};
  """
  def convert(prefix, table, column, name, old, default),
    do: fragment(@convert_enum_sql, [prefix, table, column, name, old, default])

  @create_enum_sql "create type ${1}.${2} as enum(${3});"
  def create_enum(prefix, name, values) do
    values_sql = Enum.map_join(values, ", ", &"'#{&1}'")
    fragment(@create_enum_sql, [prefix, name, values_sql])
  end

  @create_index_function_sql """
  create or replace function ${1}.${2}(${3}) returns int language sql as $$
    select case $1
      ${4}
    end
  $$;
  """
  def create_index_function(prefix, func, name, values) do
    values_sql = Enum.map_join(values, "\n    ", &"when '#{elem(&1, 0)}' then #{elem(&1, 1)}")
    fragment(@create_index_function_sql, [prefix, func, name, values_sql])
  end

  @drop_enum_sql "drop type ${1}.${2};"
  def drop_enum(prefix, name), do: fragment(@drop_enum_sql, [prefix, name])

  @drop_index_function_sql "drop function if exists ${1}.${2}(${3});"
  def drop_index_function(prefix, func, name),
    do: fragment(@drop_index_function_sql, [prefix, func, name])

  @get_enum_columns_sql """
  select col.table_schema, col.table_name, col.column_name   
    from information_schema.columns col
    join pg_type typ on col.udt_name = typ.typname
    join pg_namespace ns on ns.oid = typ.typnamespace
    where
      col.table_schema not in ('information_schema', 'pg_catalog') and
      ns.nspname = '${1}' and
      typ.typtype = 'e' and
      typ.typname = '${2}';
  """
  def get_enum_columns(prefix, name), do: fragment(@get_enum_columns_sql, [prefix, name])

  @get_values_sql "select value from unnest(enum_range(null::${1}.${2})) value;"
  def get_values(prefix, name), do: fragment(@get_values_sql, [prefix, name])

  @get_values_with_index_sql "select value, ${1}.${2}(value) from unnest(enum_range(null::${1}.${3})) value;"
  def get_values_with_index(prefix, func, name),
    do: fragment(@get_values_with_index_sql, [prefix, func, name])

  @rename_enum_sql "alter type ${1}.${2} rename to ${3};"
  def rename_enum(old_prefix, old_name, new_name),
    do: fragment(@rename_enum_sql, [old_prefix, old_name, new_name])

  @rename_value_sql "alter type ${1}.${2} rename value '${3}' to '${4}';"
  def rename_value(prefix, name, old, new),
    do: fragment(@rename_value_sql, [prefix, name, old, new])

  defp fragment(sql, args),
    do: args |> Enum.with_index(1) |> Enum.reverse() |> Enum.reduce(sql, &do_fragment/2)

  defp do_fragment({el, index}, acc) do
    pattern = :binary.compile_pattern("${#{index}}")
    String.replace(acc, pattern, el)
  end
end
