defmodule Enumex.Task do
  @moduledoc false

  @project_info [
    author: "Tomasz Marek Sulima eiji7@cryptolab.net",
    version: Mix.Project.get().project()[:version]
  ]

  def format_errors(optimus, subcommand, errors) do
    optimus
    |> Optimus.Errors.format(subcommand, errors)
    |> remove_blank_line()
    |> Enum.each(&IO.puts/1)
  end

  def help(optimus), do: optimus |> Optimus.Help.help([:enum], columns()) |> clean_lines()

  defp clean_lines(lines) do
    lines
    |> Enum.map(&String.replace_trailing(&1, " ", ""))
    |> remove_blank_line()
  end

  defp remove_blank_line(help) do
    if List.last(help) == "", do: List.delete_at(help, -1), else: help
  end

  defp columns do
    case :io.columns() do
      {:ok, width} -> width
      _ -> 80
    end
  end

  def info(name) do
    options = name |> task_options() |> Enum.map(&add_help(name, &1))

    flags = [
      help: [help: "Displays this help message.", long: "--help", short: "-h"],
      version: [help: "Displays information about version.", long: "--version", short: "-v"]
    ]

    all_flags = flags ++ task_flags(name)
    task_info = [args: task_args(name), flags: all_flags, name: name, options: options]
    subcommands = [enum: do_info(name) ++ task_info ++ @project_info]
    [name: "mix", subcommands: subcommands] ++ @project_info
  end

  def shortdoc("enumex.gen.enum"), do: "Generates an enum module."
  def shortdoc("enumex.gen.migration"), do: "Generates an enum migration."

  defp do_info("enumex.gen.enum" = name),
    do: [about: shortdoc(name), description: "Enum generator"]

  defp do_info("enumex.gen.migration" = name),
    do: [about: shortdoc(name), description: "Enum migration generator"]

  defp add_help(name, {option, info}), do: {option, [{:help, add_help(name, option)} | info]}
  defp add_help("enumex.gen.enum", :adapter), do: "Specifies adapter enum option."
  defp add_help("enumex.gen.enum", :file_path), do: "Specifies file path for enum."
  defp add_help(_task, :module), do: "Specifies an enum module."
  defp add_help("enumex.gen.enum", :prefix), do: "Specifies prefix for enum"
  defp add_help("enumex.gen.enum", :type), do: "Specifies an `ecto` type for enum."
  defp add_help("enumex.gen.enum", :values), do: "Values in format 'name' or 'name:index'"
  defp add_help("enumex.gen.migration", :repo), do: "The repo to generate migration for."

  defp task_args("enumex.gen.enum" = name) do
    help = add_help(name, :values)
    [values: [help: help, required: true, value_name: "VALUE_1, VALUE_2 … VALUE_N"]]
  end

  defp task_args("enumex.gen.migration"),
    do: [name: [help: "Migration name.", required: true, value_name: "MIGRATION_NAME"]]

  defp task_options("enumex.gen.enum") do
    [
      adapter: [long: "--adapter", short: "-a", value_name: "ADAPTER"],
      file_path: [long: "--file", short: "-f", value_name: "FILE_PATH"],
      module: [long: "--module", required: true, short: "-m", value_name: "MODULE"],
      prefix: [long: "--prefix", short: "-p", value_name: "PREFIX"],
      type: [long: "--type", short: "-t", value_name: "ECTO_TYPE"]
    ]
  end

  defp task_options("enumex.gen.migration") do
    [
      module: [long: "--module", required: true, short: "-m", value_name: "MODULE"],
      repo: [long: "--repo", required: true, short: "-r", value_name: "REPO"]
    ]
  end

  defp task_flags("enumex.gen.migration") do
    [
      no_compile: [help: "Does not compile applications before running.", long: "--no-compile"],
      no_deps_check: [
        help: "Does not check depedendencies before running.",
        long: "--no-deps-check"
      ]
    ]
  end

  defp task_flags(_task), do: []
end
