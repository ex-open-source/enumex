defmodule Enumex.Adapter do
  @moduledoc "An adapter for working with database enums."

  @typedoc "A database column name."
  @type column :: String.t()

  @typedoc "A name of function for mapping enum values."
  @type func :: String.t()

  @typedoc "An enum name."
  @type name :: String.t()

  @typedoc "A database prefix."
  @type prefix :: String.t()

  @typedoc "A string with SQL code ready for migrate/query."
  @type sql :: String.t()

  @typedoc "A database table name."
  @type table :: String.t()

  @typedoc "Type of adding value i.e. after or before"
  @type type :: String.t() | nil

  @typedoc "An enum value."
  @type value :: String.t()

  @typedoc "An enum values for SQL value mapper."
  @type values :: [{value, non_neg_integer}]

  @doc "Returns SQL for adding value to enum."
  @callback add_value(prefix, name, value, type, value | nil) :: sql

  @doc "Returns SQL for converting table column enum type (required for dropping enum values)."
  @callback convert(prefix, table, column, name, value, value) :: sql

  @doc "Returns SQL for creating a database enum."
  @callback create_enum(prefix, name, [value]) :: sql

  @doc "Returns SQL for creating function which maps enum value to its index."
  @callback create_index_function(prefix, func, name, values) :: sql

  @doc "Returns SQL for dropping function which maps enum value to its index."
  @callback drop_index_function(prefix, func, name) :: sql

  @doc "Returns SQL for dropping a database enum."
  @callback drop_enum(prefix, name) :: sql

  @doc "Returns SQL for fetching database columns of specified enum as type."
  @callback get_enum_columns(prefix, name) :: sql

  @doc "Returns SQL for fetching all enum values as list of strings."
  @callback get_values(prefix, name) :: sql

  @doc "Returns SQL for fetching all enum values as list of `Enumex.Value`."
  @callback get_values_with_index(prefix, func, name) :: sql

  @doc "Returns SQL for renaming a database enum."
  @callback rename_enum(prefix, name, name) :: sql

  @doc "Returns SQL for renaming an enum value."
  @callback rename_value(prefix, name, value, value) :: sql
end
