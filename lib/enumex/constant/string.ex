defmodule Enumex.Constant.String do
  @moduledoc false

  defmacro __using__(_opts \\ []) do
    quote unquote: false do
      parent = @module_string
      values = @values

      defmodule StringConstants do
        @moduledoc """
        Function-bsed constants.

        Similar to #{parent}, but returns strings instead of values.
        """

        module = inspect(__MODULE__)

        for %{atom: atom, string: string} = value <- values do
          @doc """
          #{atom} constant

              iex> #{module}.#{atom}()
              #{inspect(string)}
          """
          @spec unquote(atom)() :: String.t()
          defmacro unquote(atom)(), do: unquote(string)
        end
      end
    end
  end
end
