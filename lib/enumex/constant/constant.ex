defmodule Enumex.Constant do
  @moduledoc false

  defmacro __using__(_opts \\ []) do
    quote unquote: false do
      for %{atom: atom} = value <- @values do
        escaped_value = Macro.escape(value)

        @doc """
        #{atom} constant

            iex> #{@module_string}.#{atom}()
            #{inspect(value, custom_options: [raw: true])}
        """
        @spec unquote(atom)() :: Macro.expr()
        defmacro unquote(atom)(), do: Macro.escape(unquote(escaped_value))
      end
    end
  end

  def block, do: [constants(), module()]

  defp constants do
    quote unquote: false do
      map = @__values__ |> Enum.map(&{String.upcase(&1.string), &1.atom}) |> Enum.into(%{})

      @doc """
      Returns map of upcased value and atom value as constants.

          iex> #{inspect(__MODULE__)}.constants()
          #{inspect(map)}
      """
      @spec constants :: %{String.t() => atom}
      def constants, do: unquote(Macro.escape(map))
    end
  end

  defp module do
    quote unquote: false do
      defmodule Constants do
        alias Enumex.Value

        parent = __MODULE__ |> Module.split() |> Enum.drop(-1) |> Module.concat()
        @values [head | _tail] = Module.get_attribute(parent, :__values__)
        @module_string inspect(__MODULE__)

        use Enumex.Constant
        use Enumex.Constant.Atom
        use Enumex.Constant.Index
        use Enumex.Constant.String

        @moduledoc """
        Function-bsed constants.

            iex> #{@module_string}.#{head.atom}()
            #{inspect(head, custom_options: [raw: true])}
        """
      end
    end
  end
end
