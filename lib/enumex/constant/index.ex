defmodule Enumex.Constant.Index do
  @moduledoc false

  defmacro __using__(_opts \\ []) do
    quote unquote: false do
      parent = @module_string
      values = @values

      defmodule IndexConstants do
        @moduledoc """
        Function-bsed constants.

        Similar to #{parent}, but returns indexes instead of values.
        """

        module = inspect(__MODULE__)

        for %{atom: atom, index: index} = value <- values do
          @doc """
          #{atom} constant

              iex> #{module}.#{atom}()
              #{index}
          """
          @spec unquote(atom)() :: unquote(index)
          defmacro unquote(atom)(), do: unquote(index)
        end
      end
    end
  end
end
