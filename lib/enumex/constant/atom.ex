defmodule Enumex.Constant.Atom do
  @moduledoc false

  defmacro __using__(_opts \\ []) do
    quote unquote: false do
      parent = @module_string
      values = @values

      defmodule AtomConstants do
        @moduledoc """
        Function-bsed constants.

        Similar to #{parent}, but returns atoms instead of values.
        """

        module = inspect(__MODULE__)

        for %{atom: atom} = value <- values do
          @doc """
          #{atom} constant

              iex> #{module}.#{atom}()
              #{inspect(atom)}
          """
          @spec unquote(atom)() :: unquote(atom)
          defmacro unquote(atom)(), do: unquote(atom)
        end
      end
    end
  end
end
