defmodule Enumex do
  @moduledoc """
  Helper module to generate enum with support for lots of features.

  ## Usage

  Enumex is really configurable. It allows to create enums really easily:

      defmodule MyEnum do
        import Enumex

        values = [:first, :second]
        enum(values: values)
      end

  or with many options:

      defmodule MyEnum do
        import Enumex

        opts = [absinthe: [desc: "…", moduledoc: "…"], prefix: :my_prefix, type: :my_type]

        enum opts do
          value(:first, absinthe: [depracte: "…", description: "…"], index: 10)
          value(:second, absinthe: [depracte: "…", description: "…"], index: 20)
        end
      end

  ## Options

  Every option is optional. However you need to keep in mind:

  1. Enumex requires at least 2 values as less simply does not makes sense.
  2. Due to `ecto` limits enumex for now is not safely generating queries.
  3. Due to `erlang` atom count limit it's not recommend to accept values from end-user input.
  4. Values needs to be specified in at least one way using `:values` option or imported `value` macro.
  5. If you do not specify some values your enum may have less functionality. For example you would not be able to use migrations without specify `prefix`.

  Here are list of all available enum options:

  * `absinthe` - Optional documentation options with `desc` and `moduledoc` keys.
  * `adapter` - A module of adapter. This option is required in order to generate a migrations and queries.
  * `prefix` - Allows to change [prefix (schema in PostgreSQL)](https://www.postgresql.org/docs/current/ddl-schemas.html) for specified enum.
  * `type` - Allows to change type identifier. It's useful for migrations and required by `c:Ecto.Type.type/0` callback.
  * `values` - List of enum values.

  Types and default values:

  Name     | Type            | Default
  :------- | :-------------- | :------------------------------------------------
  absinthe | keyword         | [desc: "", moduledoc: "…"]
  adapter  | module          | nil
  prefix   | atom            | nil
  type     | atom            | (underscored last module part)
  values   | list(atom)      | N/A

  ## Values

  Please read `Enumex.Value` documentation for more details.
  """

  @doc false
  defmacro enum(opts, do: block) do
    quote do
      @before_compile Enumex.Value
      @before_compile Enumex.Base
      @__opts__ unquote(opts)
      is_list(@__opts__) || raise "Options should be a list!"
      @__values__ @__opts__[:values] || []
      import Enumex.Value, only: [value: 1, value: 2]
      _ = unquote(block)
    end
  end

  @doc false
  defmacro enum(do: block) do
    quote do
      require Enumex
      Enumex.enum([], do: unquote(block))
    end
  end

  @doc false
  defmacro enum(opts) do
    quote bind_quoted: [opts: opts] do
      require Enumex.Value
      @before_compile Enumex.Value
      @before_compile Enumex.Base
      @__opts__ opts
      is_list(@__opts__) || raise "Options should be a list!"
      @__values__ opts[:values]
    end
  end
end
