defmodule EnumexTest do
  use ExUnit.Case

  alias Support.Fixture.DocEnum
  alias DocEnum.{Constants, Type}
  alias Inspect.Enumex.Value

  doctest Constants
  doctest Constants.AtomConstants
  doctest Constants.IndexConstants
  doctest Constants.StringConstants
  doctest DocEnum
  doctest Type
  doctest Value

  defmodule MyEnum do
    import Enumex
    enum(values: [:first, :second])
  end

  defmodule StringMacroValuesEnum do
    import Enumex

    enum do
      value("first")
      value("second")
    end
  end

  defmodule StringOptsValuesEnum do
    import Enumex
    enum(values: ["first", "second"])
  end

  test "generates correct type" do
    assert MyEnum.Type.type() == :my_enum
  end

  test "allows string values" do
    assert StringMacroValuesEnum.string_values() == StringOptsValuesEnum.string_values()
  end

  test "raise on missing values" do
    message = "No values specified!"

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum(type: :fake)
      end
    end)
  end

  test "raise on empty enum" do
    message = "Enums cannot be empty!"

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum(values: [])
      end
    end)

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
        end
      end
    end)
  end

  test "raise on only one value" do
    message = "Valid enums requires at least 2 values!"

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum(values: [:first])
      end
    end)

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
          value(:first)
        end
      end
    end)

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
          value(:first, index: 1)
        end
      end
    end)
  end

  test "raise on negative index" do
    message = "Negative indexes are not allowed!"

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
          value(:first, index: -1)
          value(:second, index: -2)
        end
      end
    end)
  end

  test "raise on unknown value" do
    message = "Unknown value: 1"

    assert_raise(ArgumentError, message, fn ->
      defmodule FakeEnum do
        import Enumex

        enum(values: [1, 2])
      end
    end)
  end

  test "raise on mixed values" do
    message = "Found mixed value at: :first"

    assert_raise(ArgumentError, fn ->
      defmodule FakeEnum do
        import Enumex

        enum(values: [:first]) do
          value(:second, index: 0)
        end
      end
    end)

    assert_raise(ArgumentError, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
          value(:first)
          value(:second, index: 0)
        end
      end
    end)

    assert_raise(ArgumentError, fn ->
      defmodule FakeEnum do
        import Enumex

        enum do
          value(:first, index: 0)
          value(:second)
        end
      end
    end)
  end
end
