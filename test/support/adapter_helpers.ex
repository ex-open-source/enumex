defmodule Support.AdapterHelpers do
  alias Enumex.Adapters.Postgres

  def get_adapter, do: "ENUMEX_ADAPTER" |> System.get_env() |> do_get_adapter()

  defp do_get_adapter(nil), do: Postgres
  defp do_get_adapter("pg"), do: Postgres
  defp do_get_adapter(adapter), do: raise("Adapter #{adapter} is not supported!")
end
