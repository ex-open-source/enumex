defmodule Support.FileHelpers do
  @moduledoc "Based on `ecto_sql` test helpers."

  import ExUnit.Assertions

  @doc "Asserts a file was generated."
  def assert_file(file),
    do: assert(File.regular?(file), "Expected #{file} to exist, but does not")

  @doc "Asserts a file was generated and that it matches a given pattern."
  def assert_file(file, callback) when is_function(callback, 1) do
    assert_file(file)
    callback.(File.read!(file))
  end

  def assert_file(file, match), do: assert_file(file, &assert(&1 =~ match))

  @doc "Returns the `tmp_path` for tests."
  def tmp_path, do: Path.expand("../../tmp", __DIR__)

  @doc """
  Executes the given function in a temp directory
  tailored for this test case and test.
  """
  defmacro in_tmp(name \\ nil, func) do
    [tmp_path(), "#{__CALLER__.module}", "#{elem(__CALLER__.function, 0)}"]
    |> Path.join()
    |> do_in_tmp(name, func)
  end

  defp do_in_tmp(path, name, func) do
    quote bind_quoted: [func: func, name: name, path: path] do
      full_path = if is_nil(name), do: path, else: Path.join(path, name)
      File.rm_rf!(full_path)
      File.mkdir_p!(full_path)
      File.cd!(full_path, fn -> func.(full_path) end)
    end
  end
end
