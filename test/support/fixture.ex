defmodule Support.Fixture do
  defmodule DocEnum do
    import Enumex
    require Enumex.Base
    enum(adapter: Support.AdapterHelpers.get_adapter(), values: [:first, :second, :third])
  end

  defmodule AbsintheSchema do
    use Absinthe.Schema

    import_types(Support.Fixture.DocEnum.Absinthe)

    query do
      field :value, :doc_enum do
        resolve(fn _, _ -> {:ok, nil} end)
      end
    end
  end

  defmodule Context do
    def absinthe_enum_values(name) do
      query = ~s/{__type(name: "#{name}"){enumValues{name}}}/
      Absinthe.run(query, Support.Fixture.AbsintheSchema)
    end
  end
end
