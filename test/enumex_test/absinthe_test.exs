defmodule EnumexTest.Absinthe do
  use ExUnit.Case

  alias Support.Fixture.Context

  test "enum values" do
    enum_values = [%{"name" => "FIRST"}, %{"name" => "SECOND"}, %{"name" => "THIRD"}]
    expected = {:ok, %{data: %{"__type" => %{"enumValues" => enum_values}}}}
    assert Context.absinthe_enum_values("DocEnum") == expected
  end
end
