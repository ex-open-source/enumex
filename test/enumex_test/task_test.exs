defmodule EnumexTest.Task do
  use ExUnit.Case

  alias Enumex.Task

  @enum_task_help [
    "Enum generator 1.0.0",
    "Tomasz Marek Sulima eiji7@cryptolab.net",
    "Generates an enum module.",
    "",
    "USAGE:",
    "    mix enumex.gen.enum [--help] [--version] [--adapter ADAPTER] [--file FILE_PATH] --module MODULE [--prefix PREFIX] [--type ECTO_TYPE] VALUE_1, VALUE_2 … VALUE_N",
    "",
    "ARGS:",
    "",
    "    VALUE_1, VALUE_2 … VALUE_N        Values in format 'name' or 'name:index'",
    "",
    "FLAGS:",
    "",
    "    -h, --help           Displays this help message.",
    "    -v, --version        Displays information about version.",
    "",
    "OPTIONS:",
    "",
    "    -a, --adapter        Specifies adapter enum option.",
    "    -f, --file           Specifies file path for enum.",
    "    -m, --module         Specifies an enum module.",
    "    -p, --prefix         Specifies prefix for enum",
    "    -t, --type           Specifies an `ecto` type for enum."
  ]

  @enum_task_info [
    name: "mix",
    subcommands: [
      enum: [
        about: "Generates an enum module.",
        description: "Enum generator",
        args: [
          values: [
            help: "Values in format 'name' or 'name:index'",
            required: true,
            value_name: "VALUE_1, VALUE_2 … VALUE_N"
          ]
        ],
        flags: [
          help: [help: "Displays this help message.", long: "--help", short: "-h"],
          version: [
            help: "Displays information about version.",
            long: "--version",
            short: "-v"
          ]
        ],
        name: "enumex.gen.enum",
        options: [
          adapter: [
            help: "Specifies adapter enum option.",
            long: "--adapter",
            short: "-a",
            value_name: "ADAPTER"
          ],
          file_path: [
            help: "Specifies file path for enum.",
            long: "--file",
            short: "-f",
            value_name: "FILE_PATH"
          ],
          module: [
            help: "Specifies an enum module.",
            long: "--module",
            required: true,
            short: "-m",
            value_name: "MODULE"
          ],
          prefix: [
            help: "Specifies prefix for enum",
            long: "--prefix",
            short: "-p",
            value_name: "PREFIX"
          ],
          type: [
            help: "Specifies an `ecto` type for enum.",
            long: "--type",
            short: "-t",
            value_name: "ECTO_TYPE"
          ]
        ],
        author: "Tomasz Marek Sulima eiji7@cryptolab.net",
        version: "1.0.0"
      ]
    ],
    author: "Tomasz Marek Sulima eiji7@cryptolab.net",
    version: "1.0.0"
  ]

  @migration_task_help [
    "Enum migration generator 1.0.0",
    "Tomasz Marek Sulima eiji7@cryptolab.net",
    "Generates an enum migration.",
    "",
    "USAGE:",
    "    mix enumex.gen.migration [--help] [--version] [--no-compile] [--no-deps-check] --module MODULE --repo REPO MIGRATION_NAME",
    "",
    "ARGS:",
    "",
    "    MIGRATION_NAME        Migration name.",
    "",
    "FLAGS:",
    "",
    "    -h, --help             Displays this help message.",
    "    -v, --version          Displays information about version.",
    "    --no-compile           Does not compile applications before running.",
    "    --no-deps-check        Does not check depedendencies before running.",
    "",
    "OPTIONS:",
    "",
    "    -m, --module        Specifies an enum module.",
    "    -r, --repo          The repo to generate migration for."
  ]

  @migration_task_info [
    name: "mix",
    subcommands: [
      enum: [
        about: "Generates an enum migration.",
        description: "Enum migration generator",
        args: [
          name: [
            help: "Migration name.",
            required: true,
            value_name: "MIGRATION_NAME"
          ]
        ],
        flags: [
          help: [help: "Displays this help message.", long: "--help", short: "-h"],
          version: [
            help: "Displays information about version.",
            long: "--version",
            short: "-v"
          ],
          no_compile: [
            help: "Does not compile applications before running.",
            long: "--no-compile"
          ],
          no_deps_check: [
            help: "Does not check depedendencies before running.",
            long: "--no-deps-check"
          ]
        ],
        name: "enumex.gen.migration",
        options: [
          module: [
            help: "Specifies an enum module.",
            long: "--module",
            required: true,
            short: "-m",
            value_name: "MODULE"
          ],
          repo: [
            help: "The repo to generate migration for.",
            long: "--repo",
            required: true,
            short: "-r",
            value_name: "REPO"
          ]
        ],
        author: "Tomasz Marek Sulima eiji7@cryptolab.net",
        version: "1.0.0"
      ]
    ],
    author: "Tomasz Marek Sulima eiji7@cryptolab.net",
    version: "1.0.0"
  ]

  test "tasks" do
    assert Task.info("enumex.gen.enum") == @enum_task_info
    assert Task.info("enumex.gen.migration") == @migration_task_info
    list = [{@enum_task_help, @enum_task_info}, {@migration_task_help, @migration_task_info}]

    for {help, info} <- list do
      assert help == info |> Optimus.new!() |> Task.help()
    end
  end
end
