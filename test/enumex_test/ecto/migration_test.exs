defmodule EnumexTest.Ecto.MigrationTest do
  use ExUnit.Case

  alias Ecto.Migrator
  alias Enumex.Ecto.Migration
  alias Enumex.{TestRepo, Value}
  alias ExUnit.CaptureLog

  adapter = Support.AdapterHelpers.get_adapter()

  defmodule FirstVersionOfEnum do
    import Enumex
    enum(adapter: adapter, prefix: :public, type: :enum_name, values: [:first, :second])
  end

  defmodule SecondVersionOfEnum do
    import Enumex
    enum(adapter: adapter, prefix: :public, type: :enum_name, values: [:first, :second, :third])
  end

  defmodule ThirdEnumVersionOfEnum do
    import Enumex

    enum(adapter: adapter, prefix: :public, type: :enum_name) do
      value(:first, index: 0)
      value(:third, index: 2)
    end
  end

  defmodule FourthEnumVersionOfEnum do
    import Enumex

    enum(adapter: adapter, prefix: :public, type: :new_enum_name) do
      value(:first, index: 0)
      value(:second, index: 1)
    end
  end

  defmodule ExampleSchema do
    use Ecto.Schema
    alias FirstVersionOfEnum.Type
    schema("example", do: field(:sample, Type))
  end

  alias FirstVersionOfEnum.Type

  defmodule MigrationHelper do
    import Ecto.Migration, only: [repo: 0]

    def insert_records do
      repo().insert!(%ExampleSchema{sample: "first"}, log: :info)
      repo().insert!(%ExampleSchema{sample: "second"}, log: :info)
      repo().insert!(%ExampleSchema{sample: "first"}, log: :info)
    end
  end

  defmodule CustomMigration do
    use Ecto.Migration

    alias FirstVersionOfEnum, as: MyEnum
    alias MigrationHelper

    @disable_ddl_transaction true

    @adapter adapter
    @name "enum_name"
    @prefix "public"
    @repo MyEnum.Repo

    def change do
      value = &%Value{atom: &1, enum: MyEnum, index: &2, string: Atom.to_string(&1)}
      first = value.(:first, 0)
      second = value.(:second, 1)
      third = value.(:third, 2)
      first_list = [first, second]
      second_list = [first, second, third]
      third_list = [first, value.(:third, 2)]

      Migration.create_enum(@adapter, @prefix, @name, first_list)
      Migration.create_index_function(@adapter, @prefix, @name, first_list)
      table_data = :source |> ExampleSchema.__schema__() |> table()
      create(table_data, do: add(:sample, Type.type()))
      execute(&MigrationHelper.insert_records/0, fn -> :ok end)
      Migration.add_value(@adapter, @prefix, @name, @repo, third, second)
      Migration.create_index_function(@adapter, @prefix, @name, second_list)
      Migration.drop_value(@adapter, @prefix, @name, @repo, second, third)
      Migration.drop_index_function(@adapter, @prefix, @name, third_list)
      Migration.rename_value(@adapter, @prefix, @name, third, second)
      Migration.rename_enum(@adapter, @prefix, @name, "new_enum_name")
      execute(fn -> :ok end, &MigrationHelper.insert_records/0)

      execute(
        "drop table example;",
        "create table example (id serial, sample new_enum_name);"
      )

      Migration.drop_enum(@adapter, @prefix, "new_enum_name", first_list)
    end
  end

  defmodule MacroMigration do
    use Ecto.Migration

    alias MigrationHelper

    @disable_ddl_transaction true

    def change do
      second = List.last(FirstVersionOfEnum.values())
      third = List.last(SecondVersionOfEnum.values())
      FirstVersionOfEnum.Migration.create_enum()
      FirstVersionOfEnum.Migration.create_index_function()
      table_data = :source |> ExampleSchema.__schema__() |> table()
      create(table_data, do: add(:sample, Type.type()))
      execute(&MigrationHelper.insert_records/0, fn -> :ok end)
      FirstVersionOfEnum.Migration.add_value(third, second)
      SecondVersionOfEnum.Migration.create_index_function()
      SecondVersionOfEnum.Migration.drop_value(second, third)
      ThirdEnumVersionOfEnum.Migration.drop_index_function()
      ThirdEnumVersionOfEnum.Migration.rename_value(third, second)
      FirstVersionOfEnum.Migration.rename_enum("new_enum_name")
      execute(fn -> :ok end, &MigrationHelper.insert_records/0)

      execute(
        "drop table example;",
        "create table example (id serial, sample new_enum_name);"
      )

      FourthEnumVersionOfEnum.Migration.drop_enum()
    end
  end

  defmodule NonReversibleAddValueCustomEnum do
    import Enumex
    type = :non_reversible_add_value_custom_enum_name
    enum(adapter: adapter, prefix: :public, type: type, values: [:first, :second])
  end

  defmodule NonReversibleAddValueCustomMigration do
    use Ecto.Migration

    alias NonReversibleAddValueCustomEnum, as: TestEnum

    @disable_ddl_transaction true

    @adapter adapter
    @name "non_reversible_add_value_custom_enum_name"
    @prefix "public"
    @repo TestEnum.Repo

    def change do
      value = &%Value{atom: &1, enum: TestEnum, index: &2, string: Atom.to_string(&1)}
      third = value.(:third, 2)
      first_list = [value.(:first, 0), value.(:second, 1)]
      Migration.create_enum(@adapter, @prefix, @name, first_list)
      Migration.create_index_function(@adapter, @prefix, @name, first_list)
      Migration.add_value(@adapter, @prefix, @name, @repo, third)
    end
  end

  defmodule NonReversibleDropEnumCustomMigration do
    use Ecto.Migration

    @disable_ddl_transaction true

    @adapter adapter
    @name "non_reversible_drop_enum_custom_enum_name"
    @prefix "public"

    def change do
      value = &%Value{atom: &1, enum: MyEnum, index: &2, string: Atom.to_string(&1)}
      first_list = [value.(:first, 0), value.(:second, 1)]
      Migration.create_enum(@adapter, @prefix, @name, first_list)
      Migration.drop_enum(@adapter, @prefix, @name)
    end
  end

  defmodule NonReversibleDropIndexFunctionCustomMigration do
    use Ecto.Migration

    @disable_ddl_transaction true

    @adapter adapter
    @name "non_reversible_drop_index_function_custom_enum_name"
    @prefix "public"

    def change do
      value = &%Value{atom: &1, enum: MyEnum, index: &2, string: Atom.to_string(&1)}
      first_list = [value.(:first, 0), value.(:second, 1)]
      Migration.create_enum(@adapter, @prefix, @name, first_list)
      Migration.create_index_function(@adapter, @prefix, @name, first_list)
      Migration.drop_index_function(@adapter, @prefix, @name)
    end
  end

  defmodule NonReversibleAddValueMacroEnum do
    import Enumex
    type = :non_reversible_add_value_macro_enum_name
    enum(adapter: adapter, prefix: :public, type: type, values: [:first, :second])
  end

  defmodule NonReversibleAddValueMacroMigration do
    use Ecto.Migration

    @disable_ddl_transaction true

    def change do
      third = List.last(SecondVersionOfEnum.values())
      NonReversibleAddValueMacroEnum.Migration.create_enum()
      NonReversibleAddValueMacroEnum.Migration.create_index_function()
      NonReversibleAddValueMacroEnum.Migration.add_value(third)
    end
  end

  setup do: {:ok, migration_number: System.unique_integer([:positive]) + 1_000_000}

  test "logs", %{migration_number: num} do
    for migration <- [CustomMigration, MacroMigration] do
      up_output = CaptureLog.capture_log(fn -> :ok = Migrator.up(TestRepo, num, migration) end)
      up_lines = String.split(up_output, "\n")

      assert Enum.at(up_lines, 1) =~
               ~r"== Running #{num} #{inspect(migration)}.change/0 forward"

      assert Enum.at(up_lines, 3) =~
               "execute \"create type public.enum_name as enum('first', 'second');\""

      assert Enum.at(up_lines, 5) =~
               "execute \"create or replace function public.enumex_get_index_enum_name(enum_name) returns int language sql as $$\\n  select case $1\\n    when 'first' then 0\\n    when 'second' then 1\\n  end\\n$$;\\n\""

      assert Enum.at(up_lines, 7) =~ "create table example"

      assert Enum.at(up_lines, 10) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"first\"]"

      assert Enum.at(up_lines, 13) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"second\"]"

      assert Enum.at(up_lines, 16) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"first\"]"

      assert Enum.at(up_lines, 19) =~
               "select value, public.enumex_get_index_enum_name(value) from unnest(enum_range(null::public.enum_name)) value;"

      assert Enum.at(up_lines, 22) =~
               "alter type public.enum_name add value 'third';"

      assert Enum.at(up_lines, 24) =~
               "execute \"create or replace function public.enumex_get_index_enum_name(enum_name) returns int language sql as $$\\n  select case $1\\n    when 'first' then 0\\n    when 'second' then 1\\n    when 'third' then 2\\n  end\\n$$;\\n\""

      assert Enum.at(up_lines, 27) =~
               "alter type public.enum_name rename to enumex_tmp_enum_name;"

      assert Enum.at(up_lines, 30) =~ "create type public.enum_name as enum('first', 'third');"

      assert get_convert(up_lines, 33) =~ """
             alter table public.example alter column sample set data type enum_name using (
               case sample::text
                 when 'second' then 'third'
                 else sample::text
               end
             )::enum_name;
             """

      assert Enum.at(up_lines, 42) =~
               "drop function if exists public.enumex_get_index_enum_name(enumex_tmp_enum_name);"

      assert Enum.at(up_lines, 45) =~ "drop type public.enumex_tmp_enum_name;"

      assert Enum.at(up_lines, 47) =~
               "execute \"drop function if exists public.enumex_get_index_enum_name(enum_name);\""

      assert Enum.at(up_lines, 51) =~
               "execute \"alter type public.enum_name rename value 'third' to 'second';\""

      assert Enum.at(up_lines, 53) =~
               "execute \"alter type public.enum_name rename to new_enum_name;\""

      assert Enum.at(up_lines, 57) =~ "execute \"drop type public.new_enum_name;\""
      assert Enum.at(up_lines, 59) =~ ~r"== Migrated #{num} in \d.\ds"

      func = fn -> :ok = Migrator.down(TestRepo, num, migration) end
      down_output = CaptureLog.capture_log(func)
      down_lines = String.split(down_output, "\n")

      assert Enum.at(down_lines, 1) =~
               ~r"== Running #{num} #{inspect(migration)}.change/0 backward"

      assert Enum.at(down_lines, 3) =~
               "execute \"create type public.new_enum_name as enum('first', 'second');\""

      assert Enum.at(down_lines, 8) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"first\"]"

      assert Enum.at(down_lines, 11) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"second\"]"

      assert Enum.at(down_lines, 14) =~
               "INSERT INTO \"example\" (\"sample\") VALUES ($1) RETURNING \"id\" [\"first\"]"

      assert Enum.at(down_lines, 16) =~
               "execute \"alter type public.new_enum_name rename to enum_name;\""

      assert Enum.at(down_lines, 18) =~
               "execute \"alter type public.enum_name rename value 'second' to 'third';\""

      assert Enum.at(down_lines, 20) =~
               "execute \"create or replace function public.enumex_get_index_enum_name(enum_name) returns int language sql as $$\\n  select case $1\\n    when 'first' then 0\\n    when 'third' then 2\\n  end\\n$$;\\n\""

      assert Enum.at(down_lines, 23) =~
               "select value, public.enumex_get_index_enum_name(value) from unnest(enum_range(null::public.enum_name)) value;"

      assert Enum.at(down_lines, 26) =~
               "alter type public.enum_name add value 'second' before 'third';"

      assert Enum.at(down_lines, 28) =~
               "execute \"drop function if exists public.enumex_get_index_enum_name(enum_name);\""

      assert Enum.at(down_lines, 31) =~
               "alter type public.enum_name rename to enumex_tmp_enum_name;"

      assert Enum.at(down_lines, 34) =~
               "create type public.enum_name as enum('first', 'second');"

      assert get_convert(down_lines, 37) =~ """
             alter table public.example alter column sample set data type enum_name using (
               case sample::text
                 when 'third' then 'second'
                 else sample::text
               end
             )::enum_name;
             """

      assert Enum.at(down_lines, 46) =~
               "drop function if exists public.enumex_get_index_enum_name(enumex_tmp_enum_name);"

      assert Enum.at(down_lines, 49) =~ "drop type public.enumex_tmp_enum_name;"
      assert Enum.at(down_lines, 51) =~ "drop table example"

      assert Enum.at(down_lines, 53) =~
               "execute \"drop function if exists public.enumex_get_index_enum_name(enum_name);\""

      assert Enum.at(down_lines, 57) =~ "execute \"drop type public.enum_name;\""

      assert Enum.at(down_lines, 59) =~ ~r"== Migrated #{num} in \d.\ds"
    end
  end

  defp get_convert(lines, index),
    do: lines |> Enum.slice(index, 6) |> Enum.join("\n") |> Kernel.<>("\n")

  @message "cannot reverse migration command: add_value. " <>
             "You will need to explicitly define up/0 and down/0 in your migration " <>
             "or make sure that you provide arguments which allows reversing this command."

  for migration <- [NonReversibleAddValueCustomMigration, NonReversibleAddValueMacroMigration] do
    name = migration |> Macro.underscore() |> String.replace("_", " ")

    test "raise on #{name}", %{migration_number: num} do
      migration = unquote(migration)
      CaptureLog.capture_log(fn -> :ok = Migrator.up(TestRepo, num, migration) end)

      CaptureLog.capture_log(fn ->
        assert_raise Ecto.MigrationError, @message, fn ->
          Migrator.down(TestRepo, num, migration)
        end
      end)
    end
  end

  list = [NonReversibleDropEnumCustomMigration, NonReversibleDropIndexFunctionCustomMigration]

  for migration <- list do
    name = migration |> Macro.underscore() |> String.replace("_", " ")

    test "raise on #{name}", %{migration_number: num} do
      migration = unquote(migration)
      CaptureLog.capture_log(fn -> :ok = Migrator.up(TestRepo, num, migration) end)

      CaptureLog.capture_log(fn ->
        assert_raise Ecto.MigrationError, fn -> Migrator.down(TestRepo, num, migration) end
      end)
    end
  end
end
