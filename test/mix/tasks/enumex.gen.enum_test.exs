defmodule Mix.Tasks.Enumex.Gen.EnumTest do
  use ExUnit.Case

  import Mix.Tasks.Enumex.Gen.Enum, only: [run: 1]

  alias ExUnit.CaptureIO
  alias Support.FileHelpers

  require FileHelpers

  adapter = Support.AdapterHelpers.get_adapter()
  @adapter "#{inspect(adapter)}"
  @example_file_path "lib/example/file/path.ex"
  @example_module "MyApp.ExampleEnum"
  @example_prefix "example"
  @example_type "example_type"
  @values_short ["first", "second", "third"]

  @example_content """
  defmodule MyApp.ExampleEnum do
    @moduledoc "An enum generated using enumex.gen.enum mix task."

    import Enumex

    enum do
      value(:first)
      value(:second)
      value(:third)
    end
  end
  """

  @help """
  Enum generator 1.0.0
  Tomasz Marek Sulima eiji7@cryptolab.net
  Generates an enum module.

  USAGE:
      mix enumex.gen.enum [--help] [--version] [--adapter ADAPTER] [--file FILE_PATH] --module MODULE [--prefix PREFIX] [--type ECTO_TYPE] VALUE_1, VALUE_2 … VALUE_N

  ARGS:

      VALUE_1, VALUE_2 … VALUE_N        Values in format 'name' or 'name:index'

  FLAGS:

      -h, --help           Displays this help message.
      -v, --version        Displays information about version.

  OPTIONS:

      -a, --adapter        Specifies adapter enum option.
      -f, --file           Specifies file path for enum.
      -m, --module         Specifies an enum module.
      -p, --prefix         Specifies prefix for enum
      -t, --type           Specifies an `ecto` type for enum.
  """

  @version """
  1.0.0
  Tomasz Marek Sulima eiji7@cryptolab.net
  """

  test "help" do
    help = CaptureIO.capture_io(fn -> run(["--help"]) end)
    assert help == CaptureIO.capture_io(fn -> run(["-h"]) end)
    assert help == @help
    Application.put_env(:mix, :colors, enabled: false)
    mix_help = CaptureIO.capture_io(fn -> Mix.Tasks.Help.run(["enumex.gen.enum"]) end)
    assert mix_help =~ String.replace(help, "\n", "\n\n")
  end

  test "version" do
    version = CaptureIO.capture_io(fn -> run(["--version"]) end)
    assert version == CaptureIO.capture_io(fn -> run(["-v"]) end)
    assert version == @version
  end

  test "error on missing module" do
    error = CaptureIO.capture_io(fn -> run(["first:5", "second:10"]) end)

    assert error == """
           The following errors occured:
           \e[31m\e[1m- missing required options: --module(-m)\e[0m

           Try
               mix help enumex.gen.enum

           to see available options
           """
  end

  test "error on too less values" do
    error = CaptureIO.capture_io(fn -> run(["-m", @example_module]) end)

    assert error == """
           The following errors occured:
           \e[31m\e[1m- missing required arguments: VALUE_1, VALUE_2 … VALUE_N\e[0m

           Try
               mix help enumex.gen.enum

           to see available options
           """
  end

  test "new enum" do
    FileHelpers.in_tmp(fn _path ->
      run(["-m", @example_module] ++ @values_short)
      FileHelpers.assert_file("lib/my_app/example_enum.ex", @example_content)
    end)
  end

  test "new enum in custom file path" do
    FileHelpers.in_tmp(fn _path ->
      run(["-f", @example_file_path, "-m", @example_module] ++ @values_short)
      FileHelpers.assert_file(@example_file_path, @example_content)
    end)
  end

  test "new enum with long values" do
    FileHelpers.in_tmp(fn _path ->
      run(["-m", @example_module, "first:5", "second:10", "third:15"])

      FileHelpers.assert_file("lib/my_app/example_enum.ex", """
      defmodule #{@example_module} do
        @moduledoc "An enum generated using enumex.gen.enum mix task."

        import Enumex

        enum do
          value(:first, index: 5)
          value(:second, index: 10)
          value(:third, index: 15)
        end
      end
      """)
    end)
  end

  test "new enum with opts" do
    FileHelpers.in_tmp(fn _path ->
      opts = ["-a", @adapter, "-m", @example_module, "-p", @example_prefix, "-t", @example_type]
      run(opts ++ @values_short)

      FileHelpers.assert_file("lib/my_app/example_enum.ex", """
      defmodule #{@example_module} do
        @moduledoc "An enum generated using enumex.gen.enum mix task."

        import Enumex

        enum(adapter: #{@adapter}, prefix: :#{@example_prefix}, type: :#{@example_type}) do
          value(:first)
          value(:second)
          value(:third)
        end
      end
      """)
    end)
  end

  test "raise on parsing index" do
    assert_raise ArgumentError, "Cannot parse integer: nil", fn ->
      run(["-m", @example_module, "first:5", "second:10", "third:nil"])
    end
  end
end
