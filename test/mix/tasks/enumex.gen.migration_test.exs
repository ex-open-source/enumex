defmodule Mix.Tasks.Enumex.Gen.MigrationTest do
  use ExUnit.Case

  import Mix.Tasks.Enumex.Gen.Migration, only: [run: 1]

  alias Ecto.Migrator
  alias Enumex.TestRepo
  alias ExUnit.CaptureIO
  alias Support.FileHelpers

  require FileHelpers

  adapter = Support.AdapterHelpers.get_adapter()
  base_args = [adapter: adapter, prefix: :public, type: :generated_enum]

  defmodule PureEnum do
    import Enumex
    enum(values: [:first, :second])
  end

  defmodule FirstVersionOfEnum do
    import Enumex
    enum(base_args ++ [values: [:first, :second]])
  end

  defmodule SecondVersionOfEnum do
    import Enumex
    enum(base_args ++ [values: [:first, :second, :third]])
  end

  defmodule ThirdEnumVersionOfEnum do
    import Enumex

    enum(base_args ++ [values: [:first, :second, :last]])
  end

  defmodule CreateIndexMigration do
    use Ecto.Migration

    @disable_ddl_transaction true

    def change do
      SecondVersionOfEnum.Migration.create_index_function()
    end
  end

  @add_value_migration """
  defmodule Enumex.TestRepo.Migrations.AddValue do
    use Ecto.Migration

    def change do
      alias Enumex.Ecto.Migration
      Migration.add_value(#{inspect(adapter)}, "public", "generated_enum", Mix.Tasks.Enumex.Gen.MigrationTest.SecondVersionOfEnum.Repo, %Enumex.Value{absinthe: [], atom: :third, enum: Mix.Tasks.Enumex.Gen.MigrationTest.SecondVersionOfEnum, index: 2, string: "third"}, %Enumex.Value{absinthe: [], atom: :first, enum: Mix.Tasks.Enumex.Gen.MigrationTest.SecondVersionOfEnum, index: 0, string: "first"})
    end
  end
  """

  @create_enum_migration """
  defmodule Enumex.TestRepo.Migrations.CreateEnum do
    use Ecto.Migration

    def change do
      alias Enumex.Ecto.Migration
      Migration.create_enum(#{inspect(adapter)}, "public", "generated_enum", [%Enumex.Value{absinthe: [], atom: :first, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 0, string: "first"}, %Enumex.Value{absinthe: [], atom: :second, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 1, string: "second"}])
      Migration.create_index_function(#{inspect(adapter)}, "public", "generated_enum", [%Enumex.Value{absinthe: [], atom: :first, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 0, string: "first"}, %Enumex.Value{absinthe: [], atom: :second, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 1, string: "second"}])
    end
  end
  """

  @drop_value_migration """
  defmodule Enumex.TestRepo.Migrations.DropValue do
    use Ecto.Migration

    def change do
      alias Enumex.Ecto.Migration
      Migration.drop_value(#{inspect(adapter)}, "public", "generated_enum", Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum.Repo, %Enumex.Value{absinthe: [], atom: :third, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 2, string: "third"}, %Enumex.Value{absinthe: [], atom: :first, enum: Mix.Tasks.Enumex.Gen.MigrationTest.FirstVersionOfEnum, index: 0, string: "first"})
    end
  end
  """

  @empty_migration """
  defmodule Enumex.TestRepo.Migrations.Empty do
    use Ecto.Migration

    def change do
      alias Enumex.Ecto.Migration
    end
  end
  """

  @rename_value_migration """
  defmodule Enumex.TestRepo.Migrations.RenameValue do
    use Ecto.Migration

    def change do
      alias Enumex.Ecto.Migration
      Migration.rename_value(#{inspect(adapter)}, "public", "generated_enum", %Enumex.Value{absinthe: [], atom: :third, enum: Mix.Tasks.Enumex.Gen.MigrationTest.ThirdEnumVersionOfEnum, index: 2, string: "third"}, %Enumex.Value{absinthe: [], atom: :last, enum: Mix.Tasks.Enumex.Gen.MigrationTest.ThirdEnumVersionOfEnum, index: 2, string: "last"})
    end
  end
  """

  @help """
  Enum migration generator 1.0.0
  Tomasz Marek Sulima eiji7@cryptolab.net
  Generates an enum migration.

  USAGE:
      mix enumex.gen.migration [--help] [--version] [--no-compile] [--no-deps-check] --module MODULE --repo REPO MIGRATION_NAME

  ARGS:

      MIGRATION_NAME        Migration name.

  FLAGS:

      -h, --help             Displays this help message.
      -v, --version          Displays information about version.
      --no-compile           Does not compile applications before running.
      --no-deps-check        Does not check depedendencies before running.

  OPTIONS:

      -m, --module        Specifies an enum module.
      -r, --repo          The repo to generate migration for.
  """

  @version """
  1.0.0
  Tomasz Marek Sulima eiji7@cryptolab.net
  """

  test "enum versions" do
    Logger.configure(level: :error)
    {path, module} = assert_migration(FirstVersionOfEnum, "create_enum", @create_enum_migration)
    migrate(path, module, 1)
    {_path, _module} = assert_migration(FirstVersionOfEnum, "empty", @empty_migration)
    {path, module} = assert_migration(SecondVersionOfEnum, "add_value", @add_value_migration)
    migrate(path, module, 2)
    :ok = Migrator.up(TestRepo, 3, CreateIndexMigration)
    assert_migration(FirstVersionOfEnum, "drop_value", @drop_value_migration)
    assert_migration(ThirdEnumVersionOfEnum, "rename_value", @rename_value_migration)
    Logger.configure(level: :info)
  end

  defp assert_migration(module, name, expected) do
    FileHelpers.in_tmp(name, fn _path ->
      [path] = run(["-m", inspect(module), "-r", inspect(TestRepo), name])
      FileHelpers.assert_file(path, &assert(&1 == expected))
      {path, Module.concat([TestRepo, "Migrations", Macro.camelize(name)])}
    end)
  end

  defp migrate(path, module, num) do
    Code.require_file(path)
    Code.ensure_loaded(module)
    :ok = Migrator.up(TestRepo, num, module)
  end

  test "help" do
    help = CaptureIO.capture_io(fn -> run(["--help"]) end)
    assert help == CaptureIO.capture_io(fn -> run(["-h"]) end)
    assert help == @help
    Application.put_env(:mix, :colors, enabled: false)
    mix_help = CaptureIO.capture_io(fn -> Mix.Tasks.Help.run(["enumex.gen.migration"]) end)
    assert mix_help =~ String.replace(help, "\n", "\n\n")
  end

  test "version" do
    version = CaptureIO.capture_io(fn -> run(["--version"]) end)
    assert version == CaptureIO.capture_io(fn -> run(["-v"]) end)
    assert version == @version
  end

  test "fail on missing module" do
    output = CaptureIO.capture_io(fn -> run(["-r", inspect(TestRepo), "missing_module"]) end)

    assert output == """
           The following errors occured:
           \e[31m\e[1m- missing required options: --module(-m)\e[0m

           Try
               mix help enumex.gen.migration

           to see available options
           """
  end

  test "fail on missing repo" do
    module = FirstVersionOfEnum
    output = CaptureIO.capture_io(fn -> run(["-m", inspect(module), "missing_repo"]) end)

    assert output == """
           The following errors occured:
           \e[31m\e[1m- missing required options: --repo(-r)\e[0m

           Try
               mix help enumex.gen.migration

           to see available options
           """
  end

  test "fail on missing name" do
    module = FirstVersionOfEnum
    output = CaptureIO.capture_io(fn -> run(["-m", inspect(module), "-r", inspect(TestRepo)]) end)

    assert output == """
           The following errors occured:
           \e[31m\e[1m- missing required arguments: MIGRATION_NAME\e[0m

           Try
               mix help enumex.gen.migration

           to see available options
           """
  end

  test "raise on enum without database support" do
    message = "#{inspect(PureEnum.Repo)} is invalid or was compiled without database support."

    assert_raise(ArgumentError, message, fn ->
      run(["-m", inspect(PureEnum), "-r", inspect(TestRepo), "pure_enum_raise"])
    end)
  end
end
