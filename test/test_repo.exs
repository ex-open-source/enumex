defmodule Enumex.TestRepo do
  use Ecto.Repo, otp_app: :enumex, adapter: Ecto.Adapters.Postgres
end

alias Ecto.Adapters.SQL.Sandbox
alias Enumex.TestRepo
config = [database: "enumex_test", username: System.get_env("USER"), pool: Sandbox]
Application.put_env(:enumex, TestRepo, config)
Application.put_env(:enumex, :ecto_repos, [TestRepo])
{:ok, _} = Ecto.Adapters.Postgres.ensure_all_started(TestRepo.config(), :temporary)
_ = Ecto.Adapters.Postgres.storage_down(TestRepo.config())
:ok = Ecto.Adapters.Postgres.storage_up(TestRepo.config())
Enumex.TestRepo.start_link()
